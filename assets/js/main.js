//Document ready
docReady( () => {
  $('.js-car-choice').styleOptions({
    name: 'car-choice',
    // wrapperClass: 'car-choice',
    // displayClass: 'car-choice__display',
    // dropdownWrapperClass: 'car-choice__options',
    // singleOptionClass: 'car-choice__single-option'
  });
});
/*STYLE OPTIONS
@ Replaces select fields with divs
@ params
@   selector [string] / css selector - pass selector of single or multiple select fields you wish to style
*/
(function($) {
  $.fn.styleOptions = function(options) {
    if($(this).length > 0){
      //Defaults:
      var settings = $.extend({
        name: "styled-select"
      }, options);
      //Run:
      return $(this).each(function(index, select) {
        // console.log('Processing item:'); //DEBUG
        // console.log(select); //DEBUG
        //--Process select
        let this_select = $(this);
        //--Hide original select element
        // this_select.css({'height':'0','overflow':'hidden','position':'absolute','left':'-9999999999px'});
        // this_select.css('display','none');
        //--Create elements
        let overlay = $('<div class="select-overlay"></div>');
        let display = $('<div class="select-display"><span></span></div>');
        let dropdown = $('<div class="select-dropdown"></div>');
        //--Process options:
        let options = $(this).children('option');
        options.each(function(index, single_option){
          //----Add first option to display
          if(index === 0) {
            display.children('span').text($(single_option).text());
          }
          //----Add option to dropdown
          let single_option_wrap = $('<div class="select-single-option" tabindex="-1"><span>' + $(single_option).text() + '</span></div>');
          //----Dropdown click event
          single_option_wrap.click(function() {
            display.children('span').text($(this).children('span').text()); //Add to display
            //------Set select value to clicked value
            this_select.children('option').each(function(){
              if($(this).val() === $(single_option).val()) { //If curent option matches clicked value
                this_select.val($(this).val()); //Set select
                // console.log(this_select.val()); //DEBUG
              }
            });
            //------Close dropdown
            if(dropdown.css('display') !== 'none'){
              dropdown.slideUp();
            }
          });
          //----Single option keydown event
          single_option_wrap.on('keydown',function(key){
            switch(key.originalEvent.key){
              case 'ArrowDown':
                if($(this).next('.select-single-option').length > 0){
                  $(this).blur();
                  $(this).next('.select-single-option').focus();
                }
                return;

              case 'ArrowUp':
                if($(this).prev('.select-single-option').length > 0){
                  $(this).blur();
                  $(this).prev('.select-single-option').focus();
                }
                return;

              case 'Enter':
                $(this).click(); //Trigger select option event
                return;

              case 'Escape':
                $(this).blur();
                dropdown.slideUp();
                display.focus();
            }
          });
          dropdown.append(single_option_wrap);
        });
        //Process display
        //--On click
        display.click(function() {
          dropdown.slideToggle();
        });
        //--Close on click out
        $(document).mouseup(function(e){
          if(!overlay.is(e.target) && overlay.has(e.target).length === 0 && dropdown.css('display') !== 'none') {
            dropdown.slideUp();
          }
        });
        //--Make display tabbable
        display.attr('tabindex','0');
        //--Add keydown interaction
        display.on('keydown',function(key){
          switch(key.originalEvent.key){
            case 'ArrowDown':
            case 'Enter':
              if(dropdown.css('display') === 'none'){
                dropdown.slideDown(300,function(){
                  display.blur();
                  dropdown.children('.select-single-option:first-of-type').focus();
                });
              }
              return;
          }
        });
        overlay.append(display);
        //Process dropdown
        dropdown.css('display','none');
        overlay.append(dropdown);
        //Process overlay
        $('.container').append(overlay);
      });
    }
    else {
      console.warn('Selector did not return any elements. Exiting function.');
    }
  }
}(jQuery));
